from rest_framework import permissions, viewsets

from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView, RetrieveUpdateAPIView,ListCreateAPIView
from rest_framework.views import APIView
from blooddonatecore.models import Status, User
from django.contrib.auth import authenticate,login

from .serializers import StatusSerializer, UserSerializer, RegisterUserSerializer, LoginUserSerializer
from rest_framework import permissions
from rest_framework.decorators import api_view, list_route
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.http import JsonResponse, HttpResponse
from rest_framework_simplejwt.views import TokenObtainPairView
from django.contrib.auth.models import update_last_login


class CurrentUser(RetrieveUpdateAPIView):
    authentication_classes=(JWTAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class=UserSerializer
    queryset = ''
    
   
    def get(self,request,format=None):
        serializer=UserSerializer(request.user)
        content=serializer.data     
        return Response(content)
    
    def put(self,request,format=None,pk=None):
        
        serializer=UserSerializer(request.user,data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(status=201, data=serializer.data)
        return JsonResponse(status=400, data=serializer.errors)
    
    def patch(self,request,format=None,pk=None):
        serializer=UserSerializer(request.user,data=request.data,partial=True)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return JsonResponse(status=201, data=serializer.data)
        return JsonResponse(status=400, data=serializer.errors)


class StatusCreateView(CreateAPIView):
    queryset = Status.objects.all()
    serializer_class = StatusSerializer
    permission_classes = (permissions.AllowAny, )


class StatusListView(ListAPIView):
    
    serializer_class = StatusSerializer
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        
        queryset = Status.objects.all()
        blood_type = self.request.query_params.get('bloodType', None)

        if blood_type is not None:
            queryset = queryset.filter(required_bloodtype=blood_type)
            
        return queryset

class AvailableUserListView(ListAPIView):
    serializer_class=UserSerializer

    
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        
        queryset =  User.objects.all().filter(availability=True)
        blood_type = self.request.query_params.get('bloodType', None)

        


        if blood_type is not None:
            queryset = User.objects.all().  filter(availability=True,bloodtype=blood_type)
           
        return queryset


class UserDetailView(RetrieveAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

class RegisterUserView(CreateAPIView):
    authentication_classes = []
    queryset = User.objects.all()
    serializer_class=RegisterUserSerializer
    permission_classes = (permissions.AllowAny,)



    def create(self, request, *args, **kwargs):
        serializer = RegisterUserSerializer(data=request.data)
        if serializer.is_valid():
            self.perform_create(serializer)
            username = serializer.data.get('username', None)
            password = serializer.data.get('password', None)

            user = authenticate(username=username, password=password)
            
            login(request, user)
            update_last_login(None, user)
                   
                    
            
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

class LoginUserView(CreateAPIView):
    authentication_classes = []
    queryset=User.objects.all()
    serializer_class=LoginUserSerializer
    permission_classes=(permissions.AllowAny,)

   

    def post(self, request, format=None):
        
        data = request.data
        username = data.get('username', None)
        password = data.get('password', None)

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                update_last_login(None, user)
                serializer=LoginUserSerializer(request.user)
                content=serializer.data     
                return Response(content,status=200)
            else:
                return Response(status=404)
        else:
            return Response(status=404)
    
    #def get(self,request,format=None):
        