from rest_framework import serializers

from blooddonatecore.models import Status, User
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken

class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = ('id', 'user', 'required_bloodtype', 'number_of_pints_required',
                  'extra_info', 'latitude', 'longitude', 'posted_time')



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username','first_name','last_name', 'last_login','availability','bloodtype','latitude',
                  'phone_number', 'longitude', 'display_number', 'profile_image')


class LoginUserSerializer(serializers.ModelSerializer):
    '''Serializer for fetching token'''
    tokens = serializers.SerializerMethodField()
    password = serializers.CharField(write_only=True)
    class Meta:
        model = User
        fields = ('tokens','id','username','first_name','last_name','last_login', 'bloodtype', 'password', 'email','availability','latitude',
                  'phone_number', 'longitude', 'display_number', 'profile_image')
        read_only_fields = ('availability', 'latitude','bloodtype', 'first_name','last_name','last_login',
                  'phone_number', 'longitude', 'display_number', 'profile_image')
    
    def get_tokens(self, user):
        tokens = RefreshToken.for_user(user)
        refresh = str(tokens)
        access = str(tokens.access_token)
        data = {
        "refresh": refresh,
        "access": access,
        }
        return data

   
    
    
class RegisterUserSerializer(serializers.ModelSerializer):
    """Serializer for creating user objects."""

    tokens = serializers.SerializerMethodField()
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    


    class Meta:
        model = User
        fields = ('tokens','id','username','first_name','last_name', 'last_login','bloodtype', 'email','password1', 'password2','availability','latitude',
                  'phone_number', 'longitude', 'display_number', 'profile_image')
        read_only_fields = ('availability', 'latitude','bloodtype','first_name','last_name','last_login',
                  'phone_number', 'longitude', 'display_number', 'profile_image')

    def validate(self, data):
        if not data.get('password1') or not data.get('password2'):
            raise serializers.ValidationError("Please enter a password and "
                "confirm it.")

        if data.get('password1') != data.get('password2'):
            raise serializers.ValidationError("Those passwords don't match.")

        return data

    def get_tokens(self, user):
        tokens = RefreshToken.for_user(user)
        refresh = str(tokens)
        access = str(tokens.access_token)
        data = {
            "refresh": refresh,
            "access": access
        }
        return data

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username']
        )
        user.set_password(validated_data['password1'])
        user.save()    
        return user