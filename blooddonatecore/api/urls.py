from django.urls import path, include

from .views import (CurrentUser, StatusCreateView, StatusListView, UserDetailView, AvailableUserListView, RegisterUserView,LoginUserView) 

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)


urlpatterns = [

    #status list
    path('status/', StatusListView.as_view()),
    path('availableusers/', AvailableUserListView.as_view()),

    path('currentuser/',CurrentUser.as_view()),

    #createInNeedStatus
    path('status/create/', StatusCreateView.as_view()),

    #userDetail
    path('user/<pk>/', UserDetailView.as_view()),

    path('login/', LoginUserView.as_view(), name='token_obtain_pair'),
    path('login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    path('register/',RegisterUserView.as_view(), name='register_user_token' ),
    path('login/verify/', TokenVerifyView.as_view(), name='token_verify'),
   # path('')

    #update Availability
    #path('currentuser/patch', GetCurrentUser.as_view()),


   
]
