from django.contrib import admin

# Register your models here.

from .models import User, Status

admin.site.register(User)


class StatusAdmin(admin.ModelAdmin):
    list_display=('user','required_bloodtype','number_of_pints_required','extra_info')

admin.site.register(Status, StatusAdmin)

