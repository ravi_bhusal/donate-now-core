from django.db import models

from django.contrib.auth.models import AbstractUser

from django.core.validators import RegexValidator

from datetime import datetime


# Create your models here.
class User(AbstractUser):
    types_of_bloodgroup = (
        ('a', 'A+'),
        ('a-', 'A-'),
        ('b', 'B+'),
        ('b-', 'B-'),
        ('ab', 'AB+'),
        ('ab-', 'AB-'),
        ('o', 'O+'),
        ('o-', 'O-'),
    )

    bloodtype = models.CharField(
        max_length=3, choices=types_of_bloodgroup, default='a+', help_text='Blood Type')

    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(
        validators=[phone_regex], max_length=17, blank=True)  # validators should be a list

    availability = models.BooleanField(default=True)
    latitude = models.DecimalField(
        max_digits=9, decimal_places=6, blank=True, null=True)
    longitude = models.DecimalField(
        max_digits=9, decimal_places=6, blank=True, null=True)
    display_number = models.BooleanField(default=False)
    profile_image = models.ImageField(upload_to='profilepics', null=True, blank=True)


class Status(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    
    types_of_bloodgroup = (
        ('a', 'A+'),
        ('a-', 'A-'),
        ('b', 'B+'),
        ('b-', 'B-'),
        ('ab', 'AB+'),
        ('ab-', 'AB-'),
        ('o', 'O+'),
        ('o-', 'O-'),
    )

    required_bloodtype = models.CharField(
        max_length=3, choices=types_of_bloodgroup, default='a+', help_text='Required blood type')

    number_of_pints_required = models.IntegerField()


    extra_info = models.CharField(max_length=200, blank=True, null=True)

    latitude = models.DecimalField(
        max_digits=9, decimal_places=6, blank=True, null=True)

    longitude = models.DecimalField(
        max_digits=9, decimal_places=6, blank=True, null=True)

    posted_time = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.extra_info
