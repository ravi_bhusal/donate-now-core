from django.apps import AppConfig


class BlooddonatecoreConfig(AppConfig):
    name = 'blooddonatecore'
